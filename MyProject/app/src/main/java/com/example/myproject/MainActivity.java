package com.example.myproject;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.renderscript.Sampler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class MainActivity<integer, string> extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text=findViewById(R.id.textView3);
        ButOne = findViewById(R.id.button1);
        ButTwo = findViewById(R.id.button2);
        ButThree = findViewById(R.id.button3);
        ButFour = findViewById(R.id.button4);
        ButFive = findViewById(R.id.button5);
        ButSix = findViewById(R.id.button6);
        ButSeven = findViewById(R.id.button7);
        ButEight = findViewById(R.id.button8);
        ButNine = findViewById(R.id.button9);
        ButStar = findViewById(R.id.button10);
        ButZero = findViewById(R.id.button11);
        ButZapaytaya = findViewById(R.id.button12);
        ButPlus = findViewById(R.id.buttonPluss);
        ButRezault = findViewById(R.id.button13);
        ButSubstraction = findViewById(R.id.button15);
    }

    TextView text;
    Button ButOne, ButTwo, ButThree, ButFour, ButFive, ButSix, ButSeven, ButEight, ButNine, ButStar, ButZero, ButZapaytaya, ButPlus, ButRezault, ButSubstraction;
    String Operand = "";


    public void one(View view) {
        if(text.getText().equals("0")) {
            text.setText(ButOne.getText());
        }
        else
        {
            text.setText(text.getText().toString()+ButOne.getText().toString());
        }
    }

    public void two(View view) {
        if(text.getText().equals("0")) {
            text.setText(ButTwo.getText());
        }
        else
        {
            text.setText(text.getText().toString()+ButTwo.getText().toString());
        }
    }

    public void three(View view) {
        if(text.getText().equals("0")) {
            text.setText(ButThree.getText());
        }
        else
        {
            text.setText(text.getText().toString()+ButThree.getText().toString());
        }
    }

    public void four(View view) {
        if(text.getText().equals("0")) {
            text.setText(ButFour.getText());
        }
        else
        {
            text.setText(text.getText().toString()+ButFour.getText().toString());
        }
    }

    public void five(View view) {
        if(text.getText().equals("0")) {
            text.setText(ButFive.getText());
        }
        else
        {
            text.setText(text.getText().toString()+ButFive.getText().toString());
        }
    }

    public void six(View view) {
        if(text.getText().equals("0")) {
            text.setText(ButSix.getText());
        }
        else
        {
            text.setText(text.getText().toString()+ButSix.getText().toString());
        }
    }

    public void seven(View view) {
        if(text.getText().equals("0")) {
            text.setText(ButSeven.getText());
        }
        else
        {
            text.setText(text.getText().toString()+ButSeven.getText().toString());
        }
    }

    public void eight(View view) {
        if(text.getText().equals("0")) {
            text.setText(ButEight.getText());
        }
        else
        {
            text.setText(text.getText().toString()+ButEight.getText().toString());
        }
    }

    public void nine(View view) {
        if(text.getText().equals("0")) {
            text.setText(ButNine.getText());
        }
        else
        {
            text.setText(text.getText().toString()+ButNine.getText().toString());
        }
    }

    public void zero(View view) {
        if(text.getText().equals("0")) {
            text.setText(ButZero.getText());
        }
        else
        {
            text.setText(text.getText().toString()+ButZero.getText().toString());
        }
    }

    float num1 = 0;
    float result = 0;
    String value;

    public void Plus(View view) {
        value = "+";
        num1 = Float.parseFloat((text.getText().toString()));
        text.setText("0");
    }

    public void Resault(View view) {
//        switch(view.getId())
//        {
//            case R.id.buttonPluss: result =  num1 + Float.parseFloat((text.getText().toString()));text.setText(String.valueOf(result)); break;
//            default: break;
//        }

        if(value == "+")
        {
            result = num1 + Float.parseFloat((text.getText().toString()));
            text.setText(String.valueOf(result));
        }
        if(value == "-")
        {
            result = num1 - Float.parseFloat((text.getText().toString()));
            text.setText(String.valueOf(result));
        }
        if(value == "*")
        {
            result = num1 * Float.parseFloat((text.getText().toString()));
            text.setText(String.valueOf(result));
        }
        if(value == "/")
        {
            if(Double.parseDouble((text.getText().toString())) == 0)
            {
                Toast toast = Toast.makeText(getApplicationContext(), "Ошибка!!! Делить на ноль нельзя!!!", Toast.LENGTH_SHORT);
                toast.show();
            }
            else
            {
                result = num1 / Float.parseFloat((text.getText().toString()));
                text.setText(String.valueOf(result));
            }
        }
    }

    public void substrction(View view) {
        value = "-";
        num1 = Float.parseFloat((text.getText().toString()));
        text.setText("0");
    }

    public void Umnozh(View view) {
        value = "*";
        num1 = Float.parseFloat((text.getText().toString()));
        text.setText("0");
    }

    public void Zapyataya(View view) {
        if(String.valueOf((text.getText())).indexOf('.') < 0)
        {
            text.setText((text.getText() + "."));
        }
    }

    public void Delenie(View view) {
        value = "/";
        num1 = Float.parseFloat((text.getText().toString()));
        text.setText("0");
    }

    public void Clear(View view) {
        text.setText("0");
    }

    public void ClearOneSymbol(View view) {
        if(text.getText().length() == 2 && Float.valueOf(text.getText().toString()) < 0)
        {
            text.setText(text.getText().toString().substring(0, text.length() - 2));
            text.setText("0");
        }
        else
        {
            text.setText(text.getText().toString().substring(0, text.length() - 1));
        }
        if(text.getText().equals(""))
        {
            text.setText("0");
        }
    }

    public void Procent(View view) {
        result = Float.parseFloat((text.getText().toString())) / 100;
        text.setText(String.valueOf(result));
    }
}